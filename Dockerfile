FROM ubuntu:bionic

MAINTAINER Tim Weyand <tim.weyand@me.com> 

RUN apt-get update -qq \
 && cat /etc/lsb-release \
 && apt-get install -y -qq golang-go ca-certificates curl git apt-transport-https software-properties-common \
 && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add \
 && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
 && apt-get update \
 && apt-get install -y -qq docker-ce